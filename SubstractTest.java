// Mohammad Khan, 2038700
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class SubstractTest
{
    @Test
    public void objectTest()
    {
        SubtractonacciSequence seq = new SubtractonacciSequence(2, 4);
        assertEquals(2, seq.getFirst());
        assertEquals(4, seq.getSecond());
    }

    @Test
    public void sequenceTest()
    {
        Sequence seq = new SubtractonacciSequence(2, 4);
        assertEquals(6, seq.getTerm(4));
    }
}