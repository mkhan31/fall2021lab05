//Alexander Efstathakis 2043441
public class FibonacciSequence extends Sequence {

private int first;
private int second;

    public FibonacciSequence(int one, int two){
        this.first = one;
        this.second = two;
    }

    public int getFirst(){
        return this.first;
    }

    public int getSecond(){
        return this.second;
    }

    public int getTerm(int term){
        int returnValue = 0;
        int firstVal = this.first;
        int secondVal = this.second;

        if (term == 1) {
            return this.first;
        } else if (term == 2) {
            return this.second;
        } else {
            for(int i = 1; i < term - 1; i++){
                returnValue = firstVal + secondVal;
                firstVal = secondVal;
                secondVal = returnValue;
            }
        }
        return returnValue;
    }
    
    public String toString() {
        return "Fibonacci";
    }
}
