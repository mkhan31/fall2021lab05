// Mohammad Khan, 2038700
public class SkiponacciSequence extends Sequence
{
    private int n1;
    private int n2;
    private int n3;

    public SkiponacciSequence(int n1, int n2, int n3)
    {
        this.n1 = n1;
        this.n2 = n2;
        this.n3 = n3;
    }

    public int getTerm(int term) {
        int returnValue = 0;
        int firstVal = this.n1;
        int secondVal = this.n2;
        int thirdVal = this.n3;
        
        if(term == 1) {
            return this.n1;
        }
        else if(term == 2) {
            return this.n2;
        }
        else if(term == 3) {
            return this.n3;
        } else {
            for(int i = 0; i < term - 3; i++) {

                returnValue = firstVal + secondVal;
                firstVal = secondVal;
                secondVal = thirdVal;
                thirdVal = returnValue;
            }
        }
        return returnValue;
    }

    public int getFirst()
    {
        return this.n1;
    }
  
    public int getSecond()
    {
        return this.n2;
    }
    
    public int getThird()
    {
        return this.n3;
    }

    public String toString() {
        return "Skiponacci";
    }
}