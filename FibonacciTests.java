//Shay Alex Lelichev 2043812

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class FibonacciTests {
    
    @Test
    public void testGetFirst() {
        FibonacciSequence fb = new FibonacciSequence(5, 7);
        assertEquals(5, fb.getFirst());
    }

    @Test
    public void testGetSecond() {
        FibonacciSequence fb = new FibonacciSequence(6, 2);
        assertEquals(2, fb.getSecond());
    }

    @Test
    public void testGetTerm() {
        Sequence fb = new FibonacciSequence(2, 4);
        assertEquals(10, fb.getTerm(4));
    }
}
