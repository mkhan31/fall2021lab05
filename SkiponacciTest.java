//Alexander Efstathakis 2043441
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;  
public class SkiponacciTest {
    @Test public void getNumberSkiponacci(){
        SkiponacciSequence test = new SkiponacciSequence(1, 2, 3);
        assertEquals(1, test.getFirst());
        assertEquals(2, test.getSecond());
        assertEquals(3, test.getThird());
    }
    @Test public void testSequence(){
        Sequence test = new SkiponacciSequence(2, 4, 1);
        assertEquals(18, test.getTerm(9));
    }
}