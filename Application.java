import java.util.Scanner;
public class Application
{
    public static void main(String[] args)
    {
        //print(parse("Fib;1;5;x;Skip;2;2;3;Sub;2;5;x;Fib;2;3;x") , 5);
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a string for sequences:");
        String input = in.next();
        if (validate(input)) {
            print(parse(input) , 10);
        } else {
            throw new IllegalArgumentException("Please enter a valid array");
        }
    }

    public static void print(Sequence[] seq, int n) {
        for (int i = 0; i < seq.length; i++) {
            System.out.print("\n" + seq[i] + ": ");
            for (int j = 1; j <= n; j++) {
                System.out.print(seq[i].getTerm(j) + " ");
            }
        }
    }
    
    public static Sequence[] parse(String str)
    {
        String[] arrStr = str.split(";");
        Sequence[] arrSeq = new Sequence[(arrStr.length/4)];
        int seqCount = 0;
        int splitCount = 0;
        for(String s : arrStr)
        {
            if(s.equals("Fib"))
            {
                arrSeq[seqCount] = new FibonacciSequence(Integer.parseInt(arrStr[++splitCount]), Integer.parseInt(arrStr[++splitCount]));
                splitCount+=2;
                seqCount++;
            }
            else if(s.equals("Skip"))
            {
                arrSeq[seqCount] = new SkiponacciSequence(Integer.parseInt(arrStr[++splitCount]), Integer.parseInt(arrStr[++splitCount]), Integer.parseInt(arrStr[++splitCount]));
                splitCount++;
                seqCount++;
            }
            if(s.equals("Sub"))
            {
                arrSeq[seqCount] = new SubtractonacciSequence(Integer.parseInt(arrStr[++splitCount]), Integer.parseInt(arrStr[++splitCount]));
                splitCount+=2;
                seqCount++;
            }
        }
        return arrSeq;
    }

    public static boolean validate(String str)
    {
        boolean val = true;
        String[] check = str.split(";");
        if (!(check.length % 4==0))
        {
            return false;
        }
        else
        {
            for(int i = 0; i < check.length; i += 4) {
                if (check[i].equals("Fib") || check[i].equals("Sub")) {
                    try {
                        Integer.parseInt(check[i + 1]);
                        Integer.parseInt(check[i + 2]);
                    } catch(Exception e) {
                        return false;
                    } finally {
                        if (check[i + 3].equals("x")){
                            val = true;
                        } else {
                            return false;
                        }
                    }
                } else if(check[i].equals("Skip")) {
                    try {
                        Integer.parseInt(check[i + 1]);
                        Integer.parseInt(check[i + 2]);
                        Integer.parseInt(check[i + 3]);
                    } catch(Exception e) {
                        return false;
                    } finally {
                        val = true;
                    }
                } else {
                    {
                        System.out.println("2");
                        return false;
                    }
                }
            }
        }
        return val;
    }
}