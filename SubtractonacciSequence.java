//Shay Alex Lelichev 2043812

public class SubtractonacciSequence extends Sequence {

    private int firstTerm;
    private int SecondTerm;
    
    public SubtractonacciSequence(int firstParameter , int SecondParameter) {

        this.firstTerm = firstParameter;
        this.SecondTerm = SecondParameter;
    }

    public int getTerm(int term) {
        int output = this.SecondTerm;
        int middle = 0;
        int prev = this.firstTerm;
        if (term == 1) {
            return this.firstTerm;
        } else if (term == 2) {
            return this.SecondTerm;
        } else {
            for(int i = 0; i < term -2; i++) {
                middle = output;
                output = prev - output;
                prev = middle;
            }
        }
        return output;
    }

    public int getFirst() {
        return this.firstTerm;
    }

    public int getSecond() {
        return this.SecondTerm;
    }

    public String toString() {
        return "Subtractonacci";
    }
}
